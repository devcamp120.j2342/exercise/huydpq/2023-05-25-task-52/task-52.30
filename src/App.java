import java.util.ArrayList;

import p1.Person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arr = new ArrayList<>();
        // khỏi tạo Person với các tham số khác nhau

        Person person0 = new Person();
        Person person1 = new Person("Devcamp");
        Person person2 = new Person("Huy", 24, 24.88);
        Person person3 = new Person("Hùg", 24, 35.9, 5000000, new String[] { "litter camp" });
        arr.add(person0);
        arr.add(person1);
        arr.add(person2);
        arr.add(person3);

        for(Person person : arr){
            System.out.println(person.toString());
        }
    }
}
