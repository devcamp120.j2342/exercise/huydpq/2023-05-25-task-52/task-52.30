package p1;

import java.util.Arrays;

public class Person {
    String name;
    int age; // tuổi
    double weight; // cân nặng
    long salary; // lương
    String[] pets;// vật nuôi

    // khởi tạo 1 tham số name
    public Person(String name) {
        this.name = name;
    }

    // khởi tạo với tất cả tham số
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }

    // khởi tạo KO tham số name
    public Person() {
    }
// khởi tạo 3 tham số name
    public Person(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", weight=" + weight + ", salary=" + salary + ", pets="
                + Arrays.toString(pets) + "]";
    }

    

}
